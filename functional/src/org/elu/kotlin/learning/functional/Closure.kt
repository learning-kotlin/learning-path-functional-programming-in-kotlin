package org.elu.kotlin.learning.functional

fun outsideFuntion() {
    val number = 10
    unaryOperation(20, { x -> x * number })
}

fun outsideFunctionWithLoop() {
    for (number in 1..10) {
        unaryOperation(20, { x ->
            println(number)
            x * number
        })
    }
}

fun main(args: Array<String>) {
    outsideFunctionWithLoop()
}