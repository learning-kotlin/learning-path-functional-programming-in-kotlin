package org.elu.kotlin.learning.functional

fun operation(x: Int, y: Int, op: (Int, Int) -> Int): Int {
    return op(x, y)
}

fun operation(x: Int, op: (Int) -> Unit) {
    op(x)
}

fun route(path: String, vararg actions: (String, String) -> String) {}

fun unaryOperation(x: Int, op: (Int) -> Int) {
    println(op(x))
}

fun unaryOp(op: (Int) -> Int) {
}

fun sum(x: Int, y: Int) = x + y

fun transation(db: Database, code: () -> Unit) {
    try {
        code()
    } finally {
        db.commit()
    }
}

class Database {
    fun commit() {}
}

fun main(args: Array<String>) {
    println(operation(1, 2, ::sum))

    // lambda expression
    println(operation(1, 2, { x: Int, y: Int -> x + y }))
    // type is inferred
    println(operation(1, 2, { x, y -> x + y }))

    unaryOperation(2, {x -> x * x})
    // single parameters function can be simplified
    unaryOperation(3, {it * it})
    // alternative format
    unaryOperation(3) {it * it}

    unaryOp { it * it }

    val db = Database()
    transation(db) {
        // interact with database
    }

    // anonymous functions
    println(unaryOperation(4, fun(x: Int): Int { return x * x}))
}
