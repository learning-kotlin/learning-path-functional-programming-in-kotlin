package org.elu.kotlin.learning.functional

fun String.hello() {
    println("It's me!")
}

fun String.toTitleCase(): String {
    return this.split(" ").joinToString(" ") { it.capitalize() }
}

fun String.toTitleCasePrefix(prefix: String): String {
    return this.split(" ").joinToString(" ") { "$prefix ${it.capitalize()}" }
}

fun main(args: Array<String>) {
    "Hello".hello()

    val string = "Another string"
    string.hello()

    println("this is a sample string to Title Case it".toTitleCase())
    println("this is a sample string to Title Case it".toTitleCasePrefix("Again - "))

    val customer = Customer()
    customer.makePreferred()
    customer.makePreferred("My new version")

    // extension function are resolved statically
    val instance: BaseClass = InheritedClass()
    instance.extension()
    val instance2 = InheritedClass()
    instance2.extension()
}

class Customer {
    fun makePreferred() {
        println("Customer version")
    }
}
// this will not override customer member function
fun Customer.makePreferred() {
    println("Extended version")
}
fun Customer.makePreferred(message: String) {
    println(message)
}

open class BaseClass
class InheritedClass: BaseClass()

fun BaseClass.extension() {
    println("Base extension")
}
fun InheritedClass.extension() {
    println("Inherited extension")
}
